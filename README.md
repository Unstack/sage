# Sage

Base project for React/Redux + Sagas projects. Contains everything needed to
create lightning fast Redux application that work and display consistently
across a plethora of devices and screens.

Currently included are:

*Core*
- React + Redux + Sagas
  - Makes dreams come true
- store2
  - Saga-friendly state persistance

*CSS*
- Bootstrap
  - Make markup great again
- animate.css
  - All the CSS animations you could ever wish for

*Other*
- @babel/plugin-proposal-export-default-from
  - Enables use of the `export default` syntax
  - https://github.com/tc39/proposal-export-default-from
- @babel/plugin-proposal-export-namespace-from
  - Enables use of the `export * from` syntax
  - https://github.com/tc39/proposal-export-ns-from
