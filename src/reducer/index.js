import {combineReducers} from 'redux'

import UI from './UI'

const reducerMap = {
  UI
}

const rootReducer = combineReducers(reducerMap)

export default rootReducer
