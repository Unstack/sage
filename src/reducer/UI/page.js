import pages from '../../component/pages'

const initialState = {
  pages,
  active:0
}

export default (state=initialState,action) => {
  switch(action.type) {
    default:
    return state
  }
}
