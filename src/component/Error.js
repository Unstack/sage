import React from 'react';
import logo from '../logo.svg';

const Error = ({error}) => (
  <div className="container-fluid h-100 ">
        <div className="row m-0 pattern-1">
          <div className="col">
            <h1>Error</h1>
          </div>
        </div>

        <div className="row pattern-2">
          <div className="col-2">
            <img src={logo} className="h-25"/>
          </div>
          <div className="col">
            <p>
              Something broke considerably.
            </p>
          </div>
        </div>
      </div>
)

export default Error

