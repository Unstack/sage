import React from 'react'

const HomePage = () => (
    <div className="row justify-content-center pattern-1">
      <div className="col-10">
        <h1 className="h2">Home</h1>
        <hr/>
        <p className="p-2">
          Sage is a starterkit for making React + Redux applications with Sagas.
        </p>

        <p className="p-2">
          Everything you need to make immersive webapplications is included:
        </p>
      </div>
    </div>
)

export default HomePage
