import React from 'react'
import logo  from '../logo.svg'

import {Route,Redirect} from 'react-router'

import {Link} from 'react-router-dom'

import {Row,Column} from './Container'
import pages from './pages'

const App = () => (
  <div className="h-100 p-0 m-0 animated fadeIn slow" >
        <div id="backdrop" className="fixed-top"/>

        <Row className="row p-0 pattern-1 mb-2">
          <Column className="col-4">
            <h1 className="h1 font-title mt-2 p-2">
              <Link to="/home"> Sage </Link>
            </h1>
          </Column>
          <Column>
            <nav className="navbar">
              {pages.map(({name,href},key)=>(
                <Link to={"/"+href} key={key}>{name}</Link>
              ))}
            </nav>
          </Column>
        </Row>
        {
          pages.map(({href, component:Component },key) =>
                  <Route path={"/"+href} key={key} render={ ()=> < Component /> } />
                )
        }
        <Redirect from="/" to="/home" />
  </div>
)

export default App
