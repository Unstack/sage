import React,{Component} from 'react'

import { BrowserRouter } from "react-router-dom"

import {Provider} from 'react-redux'

import logo from '../logo.svg'

import Error from './Error'

class AppContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {error:null}

  }
  componentDidCatch(error, info) {
    this.setState({ error })
  }

  render() {
    return (
      this.state.error
        ? <Error error={this.state.error} />
        : <BrowserRouter>
            <Provider store={this.props.store}>
              {this.props.children}
            </Provider>
          </BrowserRouter>


    );
  }
}

export default AppContainer
