import React from 'react'

const Container = ({children, id, className, fluid,style}) => (
  <div className={(fluid?"container-fluid ":"container ")+className }
      id={id}
      style={style}
  >
    {children}
  </div>
)

const Row = ({children, id, className}) => (
  <div className={"row "+className} id={id} >
    {children}
  </div>
)


const Column = ({children, id, className}) => (
  <div className={"col "+className} id={id} >
    {children}
  </div>
)



export {Container,Row,Column}
