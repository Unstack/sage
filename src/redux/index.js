import {connect} from 'react-redux'

import mapStateToProps    from './mapstatetoprops'
import mapDispatchToProps from './mapdispatchtoprops'

const redux = connect(mapStateToProps,mapDispatchToProps)

export default redux
