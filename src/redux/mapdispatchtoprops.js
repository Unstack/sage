import * as action from '../action'

const mapDispatchToProps = (dispatch) => ({
  page_open: (page) => dispatch( action.UI.page.open(page) )
})

export default mapDispatchToProps
