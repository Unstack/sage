import { createStore,
         applyMiddleware,
         compose }                   from 'redux'
import createSagaMiddleware          from 'redux-saga'
import store2                        from 'store2'

import initialState from './initialState'

import rootReducer  from '../reducer'
import rootSaga     from '../saga'

const storeName = "applicationdata"

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const sagaMiddleware = createSagaMiddleware()

const savedState = store2(storeName) || initialState

const store = createStore( rootReducer, savedState,
  composeEnhancers(
    applyMiddleware(sagaMiddleware)
  )
)

sagaMiddleware.run(rootSaga)


export default store
