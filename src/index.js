import React        from 'react'
import ReactDOM     from 'react-dom'

import Redux        from './redux'

import AppContainer from './component/AppContainer'
import App          from './component/App'

import store        from './redux/store'

import * as         serviceWorker from './util/serviceWorker'

import './style.js'

const ReduxApp = Redux(App)

const WrappedReduxApp = () => (
  <AppContainer store={store}>
    <ReduxApp/>
  </AppContainer>
)

ReactDOM.render(<WrappedReduxApp/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
